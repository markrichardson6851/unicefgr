<!DOCTYPE html>
<html manifest="cache.manifest">
    <head>
        <title>Η σύμβαση για τα δικαιώματα του παιδιού - εκπαίδευση</title>
        <meta name="description" content="UNICEF - Η Σύμβαση για τα Δικαιώματα του Παιδιού. Ο κόσμος δεν μας προσφέρθηκε απ' τους γονείς μας, μας τον δάνεισαν τα παιδιά μας. σε τέσσερις" />
    	<meta name="keywords" content="" />
        
        
        <!--[if lt IE 9]>
            <script src="/js/html5shiv.js"></script>
            <script src="/js/mq.min.js"></script>
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    	<meta charset="UTF-8" />
        <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
        <meta name="viewport" content="width=device-width" />
        <link href="/images/favicon.ico" rel="icon" type="image/x-icon" />
		
		<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="/css/social.css" rel="stylesheet" type="text/css" />
        <noscript>
			<link rel="stylesheet" type="text/css" href="/css/nj.css" />
        </noscript>
        
        <link href="/css/my_style.css" rel="stylesheet" type="text/css" />
        
		<!--[if lt IE 9]><script>location.href="/browser.php";</script><![endif]-->
        <!--[if lte IE 9]><link href="/css/customIE.css" rel="stylesheet" type="text/css" /><![endif]-->
        <script type="text/javascript" src="/js/lib-1-9-1.js"></script><!-- lib Js -->
    </head>
<body>

<!-- Start Main Wrapper -->
<div class="wrapper">
	<!-- Start of Header -->
	<header>
		<!-- Top bar -->
		<section class="top_bar">
            <article class="top_menu">
                <ul>
                    <li  ><a href="/"><span class="icon_home"></span></a></li>
                    
                        <li  >
                            <a href="/σχετικά-με-τη-unicef/unicef/2"  >Σχετικά με τη UNICEF</a>
                        </li>
                        <li  >
                            <a href="http://inspiredgifts.unicef.gr/"  target="_blank"  >Εμπνευσμένα Δώρα</a>
                        </li>
                        <li  >
                            <a href="/κέντρο-τύπου/unicef/21"  >Κέντρο Τύπου</a>
                        </li>
                        <li  >
                            <a href="/επικοινωνία"  >Επικοινωνία</a>
                        </li>                </ul>
            </article>
            <article class="social"> 		
                <ul>
                    <li><a href="https://www.facebook.com/unicef.gr" class="facebook" target="_blank"></a></li>
                    <li><a href="https://twitter.com/unicefgreece" class="twitter" target="_blank"></a></li>
                </ul>
            </article>
		</section>
		<!-- end top bar -->
		<!-- Logo -->
		<section class="logo_container">
	        <section class="logo_pos">
                <a href="/" id="logo">Ελληνική Εθνική Επιτροπή</a>
                <div class="clear"></div>
            </section>
            <section class="search_pos">
                <div class="nav_search">
                    <h3>ενωμένοι για τα παιδιά</h3>
                    <div class="clear"></div>
                    <form name="search_form" id="search_form" action="/tag" method="post">
                        <input type="text" value="Αναζήτηση..." name="search_text" id="search_text" />
                        <button id="search_btn" ><span class="icon_search"></span></button>
                    </form>
                </div>
                <div class="clear"></div>
            </section>
		</section>
		<!-- end logo -->
		<!-- Main Nav Bar -->
        <nav id="navi">
			
	<ul class="navi">
            <li  > <a href="javascript:void(0)">ΤΟ ΕΡΓΟ ΤΗΣ UNICEF<b class="caret"></b></a>
                <ul><li><a href="/δικαιώματα-του-παιδιού/το-έργο-της-unicef/1">Δικαιώματα του Παιδιού </a></li><li><a href="/προγράμματα-βοήθειας/το-έργο-της-unicef/2">Προγράμματα Βοήθειας </a></li><li><a href="/παιδική-επιβίωση/το-έργο-της-unicef/3">Παιδική Επιβίωση </a></li><li><a href="/παιδική-προστασία/το-έργο-της-unicef/4">Παιδική Προστασία </a></li><li><a href="/εκπαίδευση/το-έργο-της-unicef/5">Εκπαίδευση </a></li><li><a href="/επείγουσες-ανάγκες/το-έργο-της-unicef/6">Επείγουσες Ανάγκες </a></li><li><a href="/μητρικός-θηλασμός/το-έργο-της-unicef/7">Μητρικός Θηλασμός </a></li>
                </ul>
            </li>
            <li  > <a href="javascript:void(0)">ΝΕΑ <b class="caret"></b></a>
                <ul><li><a href="/τελευταίες-ειδήσεις/νέα/8-2014-07">Τελευταίες ειδήσεις</a></li><li><a href="/εκθέσεις-έρευνες/νέα/9-2014-05">Εκθέσεις &amp; έρευνες</a></li><li><a href="/σημαντικές-εκστρατείες/νέα/10-2014-07">Σημαντικές Εκστρατείες</a></li><li><a href="/αληθινές-ιστορίες/νέα/11">Αληθινές ιστορίες</a></li>
                </ul>
            </li>
            <li  > <a href="javascript:void(0)">ΕΛΑΤΕ ΜΑΖΙ ΜΑΣ<b class="caret"></b></a>
                <ul><li><a href="/γίνε-νονός-της-unicef/ελάτε-μαζί-μας/13">Γίνε Νονός της UNICEF</a></li><li><a href="/γίνε-μέλος-της-unicef/ελάτε-μαζί-μας/14-2014-03">Γίνε Μέλος της UNICEF</a></li><li><a href="/περιοδικό-κοσμος/ελάτε-μαζί-μας/15">Περιοδικό ΚΟΣΜΟΣ</a></li><li><a href="/εταιρική-κοινωνική-ευθύνη/ελάτε-μαζί-μας/16-2013-01">Εταιρική Κοινωνική Ευθύνη</a></li><li><a href="/συνέργειες-για-τα-παιδιά/ελάτε-μαζί-μας/17-2014-06">Συνέργειες για τα παιδιά</a></li><li><a href="/εκδηλώσεις/ελάτε-μαζί-μας/18-2014-05">Εκδηλώσεις</a></li><li><a href="/εθελοντισμός/ελάτε-μαζί-μας/19">Εθελοντισμός</a></li><li><a href="/σημεία-πώλησης-ειδών-unicef/ελάτε-μαζί-μας/29">Σημεία Πώλησης Ειδών UNICEF</a></li>
                </ul>
            </li>
            <li class="active" > <a href="javascript:void(0)">ΕΚΠΑΙΔΕΥΣΗ &amp; ΣΥΝΗΓΟΡΙΑ<b class="caret"></b></a>
                <ul><li><a href="/η-σύμβαση-για-τα-δικαιώματα-του-παιδιού/εκπαίδευση-και-συνηγορία/22">Η Σύμβαση για τα Δικαιώματα του Παιδιού</a></li><li><a href="/σχολεία-υπερασπιστές-των-παιδιών/εκπαίδευση-και-συνηγορία/28">Σχολεία Υπερασπιστές των Παιδιών</a></li><li><a href="/τα-παιδιά-γράφουν-ζωγραφίζουν-τα-δικαιώματά-τους/εκπαίδευση-και-συνηγορία/21">Τα Παιδιά Γράφουν &amp; Ζωγραφίζουν τα Δικαιώματά τους</a></li><li><a href="/εκπαιδευτικό-υλικό/εκπαίδευση-και-συνηγορία/25">Εκπαιδευτικό Υλικό</a></li><li><a href="/το-διαδίκτυο-στην-υπηρεσία-των-παιδιών/εκπαίδευση-και-συνηγορία/23">Το Διαδίκτυο στην Υπηρεσία των Παιδιών</a></li><li><a href="/συνηγορούμε-για-τα-δικαιώματα-του-παιδιού/εκπαίδευση-και-συνηγορία/27">Συνηγορούμε για τα Δικαιώματα του Παιδιού</a></li>
                </ul>
            </li>
        <li  ><a href="/unicef-product-list.php" class="eshop">E-SHOP</a></li>
		<li class="help "> <a href="/κάντε-τη-δωρεά-σας" >ΚΑΝΤΕ &nbsp; ΤΗ &nbsp; ΔΩΡΕΑ &nbsp; ΣΑΣ</a></li>
    </ul>		</nav>
		<!-- End Main Nav Bar -->	 
	</header>
<!-- End of Header -->
	
	<!-- BreadCrumbs & cart -->
	<section id="breadcrumbs_cart">
		<section class="left_side">
			<!-- BreadCrumbs -->
			<figure id="breadcrumbs">
					<ul><li  >
								<a href="/εκπαίδευση-και-συνηγορία">Εκπαίδευση &amp; συνηγορία</a><span class="divider">/</span></li><li class="active" >
								<a href="/η-σύμβαση-για-τα-δικαιώματα-του-παιδιού/εκπαίδευση-και-συνηγορία/22"><h1 class="myH1">Η Σύμβαση για τα Δικαιώματα του Παιδιού</h1></a></li>
					</ul>
			</figure>
			<!-- End of breadcrumbs -->
		</section>
		<section class="right_side">
			<figure id="cart_on_top_menu">
				<div id="cart_dropdown" class="disabled">
					<div class="total_items"> <span class="count"></span> Προϊόντα στο καλάθι. </div>
					<a id="cart_down" href="javascript:void(0)" >
						<i class="icon-shopping-cart"></i>Cart<span class="caret"></span>
					</a>
					
					<div  id="cart_down_content">
						<ul id="cart">
						</ul>
						<div class="cart_total_checkout"> 
							<div class="mini_total_cart_price">
								Σύνολο <span >0.00 &euro; </span>
							</div> 
							<a href="/unicef-checkout.php" class="continue_shopping">Ολοκλήρωση αγοράς</a>
						</div>
					</div>
				</div>
			</figure>
		</section>
	</section>
	<!-- End of BreadCrumbs & cart -->
<!-- Page Content Container -->
<section id="content">
	<section class="left_side">
    	
		<figure id="article_post_detail"> 
			<div class="main_post"><div><img src="/uploads/filemanager/banners/mnu-d04.jpg" alt="UNICEF: Η Σύμβαση για τα Δικαιώματα του Παιδιού" width="820" height="337" /><br />Φωτογραφία ©UNICEF/NYHQ2008-0847/Isaac</div>
<h3>UNICEF - Η Σύμβαση για τα Δικαιώματα του Παιδιού</h3>
<p><strong>Ο κόσμος δεν μας προσφέρθηκε απ' τους γονείς μας, μας τον δάνεισαν τα παιδιά μας<br /></strong><em>(Αφρικανική παροιμία)</em></p>
<p>Η Σύμβαση για τα Δικαιώματα του Παιδιού είναι ο πρώτος παγκόσμιος νομικά δεσμευτικός κώδικας για τα δικαιώματα που όλα τα παιδιά πρέπει να απολαμβάνουν. Θέτει στοιχειώδεις αρχές για την ευημερία των παιδιών στα διάφορα στάδια εξέλιξής τους και αποτελείται από 54 άρθρα. Η Σύμβαση ξεκίνησε με πρωτοβουλία της Πολωνικής κυβέρνησης και της UNICEF, υιοθετήθηκε ομόφωνα απο τη Γεν. Συνέλευση του ΟΗΕ στις 20 Νοεμ. 1989 και τέθηκε σε ισχύ το 1990. Μέχρι σήμερα έχει επικυρωθεί σχεδόν από όλες τις χώρες του κόσμου, ενώ στην Ελλάδα επικυρώθηκε το 1992 (ΦΕΚ 192/2.12.92).</p>
<p>- <a href="http://www.unicef.gr/το-πλήρες-κείμενο-της-σύμβασης-για-τα-δικαιώματα-του-παιδιού/a4-366-22">Το πλήρες κείμενο της Σύμβασης</a></p>
<p>- <a href="http://www.unicef.gr/βίντεο-της-unicef-για-τα-δικαιώματα-των-παιδιών/a4-448-22">Βίντεο UNICEF για τα Δικαιώματα των Παιδιών</a> (κινούμενα σχέδια)</p>
<p>- <a href="/uploads/filemanager/PDF/info/NGO's_Report_GR_final.doc" target="_blank">Εναλλακτική Έκθεση του "Δικτύου ΜΚΟ για την παρακολούθηση της εφαρμογής της Σύμβασης για τα Δικαιώματα του Παιδιού στην Ελλάδα"</a> (αρχείο MSWord 338Kb, στην Αγγλική γλώσσα).</p>
<p style="text-align: center;">Τα άρθρα που περιέχει η Σύμβαση μπορούν να ομαδοποιηθούν <strong>σε τέσσερις ευρύτερες κατηγορίες</strong>:</p>
<table style="width: 800px; margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td><strong><img style="float: left; padding: 0px 24px 0px 0px;" src="/uploads/filemanager/banners/mnu-d07.gif" alt="Δικαιώματα επιβίωσης" width="116" height="145" />Δικαιώματα Επιβίωσης <br /></strong>Kαλύπτουν το δικαίωμα του παιδιού για ζωή και τις ανάγκες που είναι πιο βασικές για την ύπαρξή του. Αυτά συμπεριλαμβάνουν ένα επαρκές επίπεδο διαβίωσης, στέγη, διατροφή και πρόσβαση σε ιατρικές υπηρεσίες.</td>
<td><strong><img style="float: left; padding: 0px 17px 5px 17px;" src="/uploads/filemanager/banners/mnu-d08.gif" alt="Δικαιώματα προστασίας" width="116" height="132" />Δικαιώματα Προστασίας <br /></strong>Aπαιτούν τα παιδιά να προφυλλάσονται από κάθε είδους κακοποίηση, αμέλεια και εκμετάλλευση. Καλύπτουν θέματα όπως ειδική φροντίδα για προσφυγόπουλα, βασανιστήρια, κακοποίηση στο σωφρονιστικό σύστημα, ανάμειξη σε εμπόλεμες διαμάχες, εργασία ανηλίκων, χρήση ναρκωτικών και σεξουαλική εκμετάλλευση.</td>
</tr>
<tr>
<td> </td>
<td> </td>
</tr>
<tr>
<td><strong><img style="float: left;" src="/uploads/filemanager/banners/mnu-d09.gif" alt="Δικαιώματα ανάπτυξης και εξέλιξης" width="140" height="144" />Δικαιώματα Ανάπτυξης - Εξέλιξης <br /></strong>Συμπεριλαμβάνουν όλα όσα τα παιδιά χρειάζονται για να μπορέσουν να εκμεταλλευθούν στο έπακρο τις δυνατότητές τους. Για παράδειγμα το δικαίωμα της εκπαίδευσης, το παιχνίδι και η αναψυχή, οι πολιτιστικές εκδηλώσεις, η πρόσβαση σε πληροφορίες και η ελευθερία της σκέψης, της συνείδησης και της θρησκείας.</td>
<td><strong><img style="float: left;" src="/uploads/filemanager/banners/mnu-d10.gif" alt="Δικαιώματα συμμετοχής" width="150" height="184" />Δικαιώματα Συμμετοχής <br /></strong>Eπιτρέπουν στα παιδιά να παίζουν ενεργό ρόλο στις κοινωνίες και τα έθνη τους. Αυτά περικλείουν την ελευθερία να εκφράζουν γνώμη, να έχουν λόγο σε ζητήματα που αφορούν στη ζωή τους, να συμμετέχουν σε οργανώσεις και να συναθροίζονται ειρηνικά.</td>
</tr>
</tbody>
</table>
<br />
<div>Δείτε επίσης την έκδοση "<a href="/uploads/filemanager/PDF/info/media_and_children.pdf" target="_blank">Τα ΜΜΕ και τα Δικαιώματα των Παιδιών</a>"<br />ένας 64σέλιδος οδηγός για δημοσιογράφους (αρχείο pdf, 226Kb)<br /><br />Μπορείτε να βρείτε τη Σύμβαση για τα Δικαιώματα του Παιδιού σε ακόμα περισσότερες γλώσσες στη διεύθυνση:<br /><a href="http://www.unicef.org/magic/briefing/uncorc.html" target="_blank">http://www.unicef.org/magic/briefing/uncorc.html</a></div>
			</div>
		</figure>
			<section id="article_list" >
					<figure class="article">
						<h2><a href="/βίντεο-της-unicef-για-τα-δικαιώματα-των-παιδιών/a4-448-22" >Βίντεο της UNICEF για τα Δικαιώματα των Παιδιών</a></h2>
					</figure>
					<figure class="article">
						<h2><a href="/το-πλήρες-κείμενο-της-σύμβασης-για-τα-δικαιώματα-του-παιδιού/a4-366-22" >Το πλήρες κείμενο της Σύμβασης για τα Δικαιώματα του Παιδιού</a></h2>
					</figure>
			</section>	</section>
    <section class="right_side" >
        
	<figure id="right_menu">
			<ul>
					<li  class="active_right_menu"  >
						<a href="/η-σύμβαση-για-τα-δικαιώματα-του-παιδιού/εκπαίδευση-και-συνηγορία/22"> Η Σύμβαση για τα Δικαιώματα του Παιδιού </a>
					</li>
					<li  >
						<a href="/σχολεία-υπερασπιστές-των-παιδιών/εκπαίδευση-και-συνηγορία/28"> Σχολεία Υπερασπιστές των Παιδιών </a>
					</li>
					<li  >
						<a href="/τα-παιδιά-γράφουν-ζωγραφίζουν-τα-δικαιώματά-τους/εκπαίδευση-και-συνηγορία/21"> Τα Παιδιά Γράφουν &amp; Ζωγραφίζουν τα Δικαιώματά τους </a>
					</li>
					<li  >
						<a href="/εκπαιδευτικό-υλικό/εκπαίδευση-και-συνηγορία/25"> Εκπαιδευτικό Υλικό </a>
					</li>
					<li  >
						<a href="/το-διαδίκτυο-στην-υπηρεσία-των-παιδιών/εκπαίδευση-και-συνηγορία/23"> Το Διαδίκτυο στην Υπηρεσία των Παιδιών </a>
					</li>
					<li  >
						<a href="/συνηγορούμε-για-τα-δικαιώματα-του-παιδιού/εκπαίδευση-και-συνηγορία/27"> Συνηγορούμε για τα Δικαιώματα του Παιδιού </a>
					</li>
			</ul>
		<div class="clear" ></div>
	</figure>
	<div class="clear"></div>            </section>
</section>
<!-- Page Content Container -->
    <!-- Start of Footer -->
        <footer id="footer" >
            <!-- Start of Footer 1 -->
            <section class="footer1">
                <figure class="footer_left">
                    <div>
                        <span class="icon_phone"> +30 210 72 55 555 </span> 
                        <span class="icon_fax"> +30 210 72 52 555 </span> 
                        <span class="icon_email"> info@unicef.gr </span>
                    </div>
                </figure>
                <figure class="footer_right">
                    <div id="socialicons">
                        <a href="https://www.facebook.com/unicef.gr" target="_blank" data-placement="top" rel="tooltip" title="Join us on Facebook" id="social_facebook" class="social_active" ><span></span></a> 
                        <a href="https://twitter.com/unicefgreece"  target="_blank" data-placement="top" rel="tooltip" title="Follow us on Twitter" id="social_twitter" class="social_active" ><span></span></a> 
                    </div>
                </figure>
            </section>
            <!-- End of Footer 1 -->
            <!-- Start of Footer 2 -->
            <section class="footer2">
                <figure class="footer_left">
                    <h3>ΕΛΛΗΝΙΚΗ ΕΘΝΙΚΗ ΕΠΙΤΡΟΠΗ UNICEF</h3>
                    <address>Ανδρ. Δημητρίου 8 &amp; Τζ. Κέννεντυ 37 16121, Καισαριανή</address>
                </figure>
                <figure class="footer_right">
                    <div>Copyright &copy; 2014 - unicef.gr</div>
                    <div class="webart"><a href="http://www.webart.gr" target="_blank">κατασκευή ιστοσελίδων webart.gr</a></div>
                </figure>
            </section>
            <!-- End of Footer  -->
        </footer>
        <div class="clear"></div>
    <!-- End of Footer -->
    </div>
    <!-- End Main Wrapper -->
    
    <!-- FACEBOOK BOX CODE RIGHT -->
    <div id="facebook_box_code">
        <div id="fixme" class="fbox">
            <div id="openclosefacebook"></div>
            <div class="fb-like-box" data-href="http://www.facebook.com/unicef.gr" data-width="350" data-show-faces="false" data-stream="true" data-header="false"></div>
            <div id="fb-root"></div>
        </div>
    </div>
    <!-- END FACEBOOK BOX CODE RIGHT -->
    
    <!-- JS Files Start -->
    <script type="text/javascript" src="/js/jquery.zoom.min.js"></script><!-- zoom -->
    <script type="text/javascript" src="/js/jquery.fancybox.pack.js?v=2.1.5"></script><!-- fancybox -->
    <script type="text/javascript" src="/js/jquery.fancybox-media.js?v=1.0.6"></script><!-- fancybox video -->
    
	<script type="text/javascript" src="/js/social.js"></script><!-- Social Media Hover Effect -->
	<script type="text/javascript" src="/js/modernizr.min.js"></script><!-- Modernizr -->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script><!-- Bootstrap -->
    <script type="text/javascript" src="/js/bxslider.min.js"></script><!-- BX Slider -->
	<script type="text/javascript" src="/js/custom.js"></script><!-- Custom / Functions -->
    
	    
    
    
	<!-- smooth Scroll -->
    
    <!--[if IE 8]>
         <script src="js/ie8_fix_maxwidth.js" type="text/javascript"></script>
    <![endif]-->
        
    </body>
</html>
