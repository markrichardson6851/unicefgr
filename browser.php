<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>UNICEF</title>
        <meta name="description" content="" />
        <link rel="canonical" href="http://www.voicesofchildren.eu">
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
        <style>
            @import url(https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400italic,700,700italic&subset=latin,greek);

            *{margin: 0;padding: 0;
            }
            html {
                font-family: Times, serif;
                font-size: 1em;
                line-height: 1.5;
                background-color: white;
                color: black;
                -moz-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
            }
            html, body{
                height: 100%;
            }
            h1{
                font-family: "Roboto Condensed", sans-serif;
                margin-bottom: 40px;
                font-size: 40px;
            }
            p{
                font-family: "Roboto Condensed", sans-serif;
                padding: 10px;
                line-height: normal

            }
            .page {
                min-height: 100%;
            }
            ul{overflow: hidden;}
            li{float: left;width: 20%;}
            a{display: block;overflow: hidden;}
            section{
                text-transform: uppercase;
                text-align: center;
                position: absolute;
                top: 50%;left: 50%;
                width: 900px;
                height: 460px;
                margin-left: -450px;
                margin-top: -230px;
            }
            
            ul li {
                list-style:none;
            }
            ul li a img {
                border:0;
            }
            a{
                color:#000;
                text-decoration: none;
            }
            a:hover{
                text-decoration: underline;
            }

        </style>
    </head>
    <body>
            <div class="page">    
                <section>        
                    <img src="images/logo_blue.png" ></img>
                    <p>Αυτή η ιστοσελίδα απεικονίζεται καλύτερα με Internet Explorer 10 ή παραπάνω έκδοση, καθώς και με Firefox, Chrome ή Safari. Παρακαλούμε αναβαθμίστε το πρόγραμμα περιήγησης σας. </p>
                    <ul>
                        <li><a href="https://www.google.com/intl/en/chrome/browser/" target="_blank">
                                <img src="images/browser/chrome.jpg" alt="">
                                <p>Visit website for more info</p>
                            </a></li>
                        <li><a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank">
                                <img src="images/browser/firefox.jpg" alt="">
                                <p>Visit website for more info</p>
                            </a></li>
                        <li><a href="https://www.apple.com/safari/" target="_blank">
                                <img src="images/browser/safari.jpg" alt="">
                                <p>Visit website for more info</p>

                            </a></li>
                        <li><a href="http://www.opera.com/en/computer/windows" target="_blank">
                                <img src="images/browser/opera.jpg" alt="">
                                <p>Visit website for more info</p>

                            </a></li>
                        <li><a href="http://windows.microsoft.com/en-us/internet-explorer/ie-10-worldwide-languages" target="_blank">
                                <img src="images/browser/explorer.jpg" alt="">
                                <p>Visit website for more info</p>

                            </a></li>
                    </ul>
                </section>
            </div>

    </body>
</html>


